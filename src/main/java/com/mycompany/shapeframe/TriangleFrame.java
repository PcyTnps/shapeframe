package com.mycompany.shapeframe;


import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author lenovo
 */
public class TriangleFrame extends JFrame {
    
    JLabel lblBase;
    JTextField txtBase;
    JLabel lblHeight;
    JTextField txtHeight;
    JButton btnCalculate;
    JLabel lblResult;

    public TriangleFrame() {
        super("Triangle");
        this.setSize(400, 300);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);

        lblBase = new JLabel("base", JLabel.TRAILING);
        lblBase.setSize(50, 20);
        lblBase.setLocation(5, 5);
        lblBase.setBackground(Color.WHITE);
        lblBase.setOpaque(true);

        txtBase = new JTextField();
        txtBase.setSize(50, 20);
        txtBase.setLocation(60, 5);
        
        lblHeight = new JLabel("height", JLabel.TRAILING);
        lblHeight.setSize(50, 20);
        lblHeight.setLocation(5, 30);
        lblHeight.setBackground(Color.WHITE);
        lblHeight.setOpaque(true);

        txtHeight = new JTextField();
        txtHeight.setSize(50, 20);
        txtHeight.setLocation(60, 30);


        btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(120, 20);

        lblResult = new JLabel("TriangleFrame base = ???, height = ???, Area = ???, Perimeter = ??");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(400, 50);
        lblResult.setLocation(0, 50);
        lblResult.setBackground(Color.MAGENTA);
        lblResult.setOpaque(true);

        btnCalculate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String strBase = txtBase.getText();
                    double base = Double.parseDouble(strBase);
                    String strHeight = txtHeight.getText();
                    double height = Double.parseDouble(strHeight);
                    Triangle triangle = new Triangle(base,height);
                    lblResult.setText("TriangleFrame base = " + triangle.getBase()+" height = " + triangle.getHeight()+ " Area = " + String.format("%.2f", triangle.calArea()) + " Perimeter = " + String.format("%.2f", triangle.calPerimeter()));
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(TriangleFrame.this, "Error : Please input nimber", "Error", JOptionPane.ERROR_MESSAGE);
                    txtBase.setText("");
                    txtBase.requestFocus();
                    txtHeight.setText("");
                    txtHeight.requestFocus();
                }
            }
        });

        this.add(lblResult);
        this.add(btnCalculate);
        this.add(txtBase);
        this.add(lblBase);
        this.add(txtHeight);
        this.add(lblHeight);
    }
    public static void main(String[] args) {
        TriangleFrame frame = new TriangleFrame();
        frame.setVisible(true);
    }
}
